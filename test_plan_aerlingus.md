# Scope
Test the script aerlingus_script.py for the listed features in contexts including different browsers, languages, numbers of flights, types of fares, and timezones. The scope of these contexts for the purposes of this test plan is as follows:
**Browsers:**
Latest versions of Firefox, Safari, Edge, and Chrome
**languages**
English and French
**Numbers of Flights:**
One way and round trip
**Types of Fares:**
Economy, Economy Flex, Business
**Timezones:**
Flights from Irish Standard Time (UTC+1:00) to Central European Time (UTC+2:00)



# Acceptance Criteria
* For each test, the script should provide the following data about a flight booking, delivered in valid JSON in a HTTP POST request to https://api.capturedata.ie:
    * For each flight:
        * Departure Time
        * Arrival Time
        * Departure Airport
        * Arrival Airport
        * Fare
        * Price
        * Date String
        * Flight Number
    * Total Price of Trip

# Test Instructions
The following steps detail the process for each of the tests to be run as part of this test plan:
1. Navigate to https://www.aerlingus.com on the selected browser
2. Search for flights according to test case:
    1. Select appropriate language using the drop down at the top of the page
    2. Select the appropriate 'From' and 'To' airports
    3. Select departure and return dates - select One Way if there is no return date
    4. Select number of passengers
3. Select a flight for each day with the relevant fare type
4. Open the browser console (F12 on most browsers)
5. Open the browser feature to run javascript code (Stratchpad (Shift+F4) on Firefox, 'Snippets' on Chrome)
6. Run the aerlingus_script.js file
7. Check that the POST request contains a valid JSON payload with the correct data.
    * Some browsers may not show the payload, in which case the console output will have a copy of it.

# Test Cases
**1.**
Browser: For all browsers in scope
Language: English
From Airport: Dublin (DUB)
To Airport: Vienna (VIE)
Departure Date: 04/07/2019
Return Date: 11/07/2019
Fare Type: Economy

**2.**
Browser: Firefox
Language: For all languages in scope
From Airport: Dublin (DUB)
To Airport: Vienna (VIE)
Departure Date: 04/07/2019
Return Date: None
Fare Type: Economy

**3.**
Browser: Firefox
Language: French
From Airport: Dublin (DUB)
To Airport: Vancover (YVR)
Departure Date: 24/12/2019
Return Date: 09/01/2020
Fare Type: For all fare types in scope
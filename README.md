# Boxever Technical Consultant Exam

## Instructions for use:

**aerlingus_script.js**
This is written to parse the initial flight summary view, immediately after the flight fare types have been selected. More detailed instructions can be found in test_plan_aerlingus.md if necessary.

**extract_transform.py**
Written using Python 3
It will output an exception each time the HTTP request fails, and ultimately write all the JSON data to a file called data.json

**test_plan_aerlingus.md**
Written as a Markdown document for ease of reading.


*Thanks a mil for your time!*


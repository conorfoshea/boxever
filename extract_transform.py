import csv
import json
import requests

API_ENDPOINT = "https://api.capturedata.ie"

# Open the input file
with open('input_data.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    passenger_list = []

    # Add data from each row to passenger_list and attempt to post data to endpoint
    for row in reader:
        passenger_list.append(row)
        try:
            r = requests.post(url=API_ENDPOINT , data=row)
        except Exception as e:
            print("POST to endpoint failed.\n" + str(e))

# Finally print contents of passenger_list to output file
with open('data.json', 'w') as outfile:
    json.dump(passenger_list, outfile, indent=4)
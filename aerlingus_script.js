API_ENDPOINT = "https://api.capturedata.ie"

function getFlightDepartureTime(flight_element) {
    return $(flight_element).find('div[data-test-id="test_fsrp_row_departure_time"]')[0].innerText
}

function getFlightArrivalTime(flight_element) {
    return $(flight_element).find('div[data-test-id="test_fsrp_row_arrival_time"]')[0].innerText
}

function getFlightDepartureAirport(flight_element) {
    return $(flight_element).find('div[data-test-id="test_fsrp_row_departure_airport"]')[0].innerText
}

function getFlightArrivalAirport(flight_element) {
    return $(flight_element).find('div[data-test-id="test_fsrp_row_arrival_airport"]')[0].innerText
}

function getFlightFare(flight_element) {
    return $(flight_element).find('div.selected-fare-info .m')[0].innerText.trim()
}

function getFlightPrice(flight_element) {
    return $(flight_element).find('div.selected-fare-info span.b.xl1')[0].innerText
}

function getFlightDate(flight_element) {
    return $(flight_element).find('div.selected-fare-info span.s')[0].innerText
}

/* Uses a regular expression which matches the AerLingus flight number structure to extract the flight number */
function getFlightNumber(flight_element) {
    const regular_expression = /EI \d{3,4}/;
    const matches = regular_expression.exec(flight_element.innerText);
    return matches[0]
}

function getTripTotalPrice(summary_element) {
    return $(summary_element).find('span[ng-show="dataObj.totalSummary.totalDue"]')[0].innerText
}

$( document ).ready(function() {
    
    var trip = {}
    trip['flights'] = []
    
    /* Obtain flight data for each flight in summary list */
    const flights = $('div.flight-details-wrap')
    flights.each(function() {
        var flight = {}

        flight['departure_time'] = getFlightDepartureTime(this)
        flight['arrival_time'] = getFlightArrivalTime(this)
        flight['departure_airport'] = getFlightDepartureAirport(this)
        flight['arrival_airport'] = getFlightArrivalAirport(this)
        flight['fare'] = getFlightFare(this)
        flight['price'] = getFlightPrice(this)
        flight['date'] = getFlightDate(this)
        flight['number'] = getFlightNumber(this)

        trip['flights'].push(flight)
    });

    /* Obtain total price for the trip */
    const summary = $('ul[data-test-id="trip_summary_button"]')
    trip['total_price'] = getTripTotalPrice(summary)

    /* Log data to console and post to API endpoint */
    console.log(JSON.stringify(trip))
    $.post( API_ENDPOINT, JSON.stringify(trip) );
});